package com.example.myapplication;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

public class AlbumActivity extends AppCompatActivity {

    private static final String LOG_TAG = "ALBUM";
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final boolean USE_INTENT_SERVICE = true;

    private String cameraFileName;
    private AlbumAdapter albumAdapter;
    private String albumDirectoryPath;
    private final BroadcastReceiver photosReceiver =  new PhotosReceiver();
    private final ServiceConnection serviceConnection = new ServiceConnection();
    private MyService.Binder photoService;

    private class PhotosReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(LOG_TAG, "Received photos");
            Collection<Photo> photos = intent.getParcelableArrayListExtra(MyIntentService.EXTRA_PHOTOS);
            AlbumActivity.this.showPhotos(photos);
        }
    }

    private class ServiceConnection implements android.content.ServiceConnection {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.i(LOG_TAG, "Service connected");
            AlbumActivity.this.photoService = (MyService.Binder) service;
            AlbumActivity.this.photoService.loadPhotos(new PhotosHandler());
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.i(LOG_TAG, "Service disconnected");
        }
    }

    private class PhotosHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            Log.i(LOG_TAG, "Handle message. Thread: " + Thread.currentThread().getName());
            switch(msg.what) {
                case MyService.LOAD_PHOTOS_MESSAGE:
                    Collection<Photo> photos = (Collection<Photo>) msg.obj;
                    showPhotos(photos);
                    break;
                case MyService.ADD_PHOTO_MESSAGE:
                    Toast toast = Toast.makeText(AlbumActivity.this, "Photo added", Toast.LENGTH_SHORT);
                    toast.show();
                    break;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(LOG_TAG, "Creating album activity");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);

        this.albumDirectoryPath = this.getIntent().getStringExtra(Intents.EXTRA_ALBUM_DIRECTORY);

        // Plug behaviour of the button to take a picture
        this.findViewById(R.id.takePictureButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    // Create file name from current date
                    cameraFileName = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ".jpg";
                    // Create URI pointing to where we want the image to be written
                    File photoFile = new File(albumDirectoryPath, cameraFileName);
                    Log.i(LOG_TAG, "Taking picture at " + photoFile.getAbsolutePath());
                    Uri photoURI = Uri.parse("file://" + photoFile.getAbsolutePath());
//                    Uri photoURI = FileProvider.getUriForFile(AlbumActivity.this,
//                            "com.example.android.fileprovider",
//                            photoFile);
                    Log.i(LOG_TAG, "URI " + photoURI);
                    // Configure the intent
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    // Trigger the camera activity
                    AlbumActivity.this.startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
        });

        // Start service to load photos
        if (USE_INTENT_SERVICE) {
            MyIntentService.startActionLoadPhotos(this);
        } else {
            bindService(new Intent(this, MyService.class), this.serviceConnection, Context.BIND_AUTO_CREATE);
        }

        // Register receiver for broadcast photos
        this.registerReceiver(this.photosReceiver, new IntentFilter(MyIntentService.ACTION_LOAD_PHOTOS));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.album_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.testMenuItem:
                Toast.makeText(this, "Youpi", Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showPhotos(Collection<Photo> photos) {
        this.albumAdapter = new AlbumAdapter(this, photos, albumDirectoryPath);
        GridView grid = this.findViewById(R.id.albumGrid);
        grid.setAdapter(this.albumAdapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Photo newPhoto = new Photo(this.albumAdapter.getCount(), this.cameraFileName, this.cameraFileName);
            this.albumAdapter.addPhoto(newPhoto);
            if (USE_INTENT_SERVICE) {
                MyIntentService.startActionAddPhoto(this, newPhoto);
            } else {
                photoService.addPhoto(newPhoto, new PhotosHandler());
            }
        }
    }

    @Override
    protected void onPause() {
        Log.i(LOG_TAG, "Pausing album activity");
        super.onPause();
    }

    @Override
    protected void onResume() {
        Log.i(LOG_TAG, "Resuming album activity");
        super.onResume();
    }

    @Override
    protected void onStop() {
        Log.i(LOG_TAG, "Stopping album activity");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.i(LOG_TAG, "Destroying album activity");
        super.onDestroy();
        this.unregisterReceiver(this.photosReceiver);
        if (!USE_INTENT_SERVICE) {
            this.unbindService(this.serviceConnection);
        }
    }
}
