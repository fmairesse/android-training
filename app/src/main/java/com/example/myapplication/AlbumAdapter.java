package com.example.myapplication;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

class AlbumAdapter extends BaseAdapter {
    private static final String LOG_TAG = "ALBUM_ADAPTER";
    private static final int MULTIPLIER = 1;

    private final Context context;
    private final List<Photo> photos;
    private final String imageDirectory;

    AlbumAdapter(Context context, Collection<Photo> photos, String imageDirectory) {
        this.context = context;
        this.photos = new ArrayList<>(photos);
        this.imageDirectory = imageDirectory;
    }

    public void addPhoto(Photo photo) {
        this.photos.add(photo);
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return this.photos.size() * MULTIPLIER;
    }

    @Override
    public Object getItem(int position) {
        return this.getPhoto(position);
    }

    @Override
    public long getItemId(int position) {
        return this.getPhoto(position).id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        // Create view from XML layout
        if (convertView == null) {
            view = LayoutInflater.from(this.context).inflate(R.layout.photo_item, null);
        } else {
            // Recycle old view
            view = convertView;
        }
        // Find imageView in the layout
        ImageView imageView = view.findViewById(R.id.imageView);
        // Remove previous displayed bitmap from imageView
        imageView.setImageBitmap(null);
        // Get the photo to display
        Photo photo = this.getPhoto(position);
        // Get file for the photo
        File imageFile = new File(this.imageDirectory, photo.fileName);
        if (imageFile.exists()) {
//            // Load a Bitmap from the image file
//            Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
//            // Give the bitmap to the image view
//            imageView.setImageBitmap(bitmap);
            new ImageLoader(imageView, imageView.getMeasuredWidth(), imageView.getMeasuredHeight()).execute(imageFile.getAbsolutePath());
        } else {
            Log.w(LOG_TAG, "Photo does not exist: " + imageFile.getAbsolutePath());
        }

        // Set the title
        TextView text = view.findViewById(R.id.photoTitle);
        text.setText(photo.title);

        // Popup menu
        PopupMenuClickListener l = new PopupMenuClickListener(photo);
        view.setOnLongClickListener(l);

        return view;
    }

    private Photo getPhoto(int position) {
        return this.photos.get(position % this.photos.size());
    }

    private static class ImageLoader extends AsyncTask<String, Integer, Bitmap> {
        private final WeakReference<ImageView> imageView;
        private final int width;
        private final int height;

        private ImageLoader(ImageView imageView, int width, int height) {
            // Use a weak ref because we might have finished to load the bitmap
            // after the activy has finished and the image view is not valid
            this.imageView = new WeakReference<>(imageView);
            this.width = width;
            this.height = height;
        }

        // Appelé dans un thread à part
        @Override
        protected Bitmap doInBackground(String... params) {
            String imagePath = params[0];
            ImageView imageView = this.imageView.get();
            Bitmap bitmap = null;
            if (imageView != null) {
                bitmap = loadThumbnail(imagePath, this.width, this.height);
            }
            return bitmap;
        }

        // Appelé dans le thread UI
        @Override
        protected void onPostExecute(Bitmap bitmap) {
            ImageView imageView = this.imageView.get();
            if (imageView != null) {
                imageView.setImageBitmap(bitmap);
            }
        }

        private static Bitmap loadThumbnail(String filePath, int width, int height) {
            Bitmap bitmap;

            // Try to load thumbnail from exif data
            byte[] thumbnail = null;
            try {
                ExifInterface exifInterface = new ExifInterface(filePath);
                thumbnail = exifInterface.getThumbnail();
            } catch (IOException e) {
            }

            // Scale image if thumbnail could not be loaded
            if (thumbnail == null) {
                // First decode with inJustDecodeBounds=true to check dimensions
                final BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(filePath, options);

                // Calculate inSampleSize
                if (width != 0 && height != 0) {
                    options.inSampleSize = calculateInSampleSize(options, width, height);
                } else {
                    Log.w(LOG_TAG, "Invalid dimension for scaling image: " + filePath);
                }
                // Load in low quality
                options.inPreferredConfig = Bitmap.Config.ARGB_4444;

                // Decode bitmap with inSampleSize set
                options.inJustDecodeBounds = false;
                bitmap = BitmapFactory.decodeFile(filePath, options);
            } else {
                // Create bitmap from byte array
                bitmap = BitmapFactory.decodeByteArray(thumbnail, 0, thumbnail.length);
            }

            return bitmap;
        }

        private static int calculateInSampleSize(
                BitmapFactory.Options options, int reqWidth, int reqHeight) {
            // Raw height and width of image
            final int height = options.outHeight;
            final int width = options.outWidth;
            int inSampleSize = 1;

            if (height > reqHeight || width > reqWidth) {

                final int halfHeight = height / 2;
                final int halfWidth = width / 2;

                // Calculate the largest inSampleSize value that is a power of 2 and keeps both
                // height and width larger than the requested height and width.
                while ((halfHeight / inSampleSize) >= reqHeight
                        && (halfWidth / inSampleSize) >= reqWidth) {
                    inSampleSize *= 2;
                }
            }

            return inSampleSize;
        }
    }

    private class PopupMenuClickListener implements View.OnLongClickListener, PopupMenu.OnMenuItemClickListener {
        private final Photo photo;

        private PopupMenuClickListener(Photo photo) {
            this.photo = photo;
        }

        @Override
        public boolean onLongClick(View v) {
            PopupMenu popup = new PopupMenu(AlbumAdapter.this.context, v);
            popup.inflate(R.menu.photo_menu);
            popup.setOnMenuItemClickListener(this);
            popup.show();
            return true;
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.deletePhotoMenuItem:
                    MyIntentService.startActionDeletePhoto(AlbumAdapter.this.context, this.photo);
                    AlbumAdapter.this.photos.remove(AlbumAdapter.this.photos.indexOf(photo));
                    break;
            }
            return true;
        }
    }
}
