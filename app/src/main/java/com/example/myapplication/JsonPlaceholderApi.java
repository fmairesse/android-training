package com.example.myapplication;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;

class JsonPlaceholderPhoto {
    long id;
    String title;
    String url;
}


public interface JsonPlaceholderApi {
    @GET("photos")
    Call<List<JsonPlaceholderPhoto>> getPhotos();

    @GET("/600/{imageName}")
    Call<ResponseBody> getImage(@Path("imageName") String imageName);

}