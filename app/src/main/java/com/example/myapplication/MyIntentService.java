package com.example.myapplication;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class MyIntentService extends IntentService {

    private static String LOG_TAG = "SERVICE";

    public static final String ACTION_LOAD_PHOTOS = "com.example.myapplication.action.LOAD_PHOTOS";
    private static final String ACTION_ADD_PHOTO = "com.example.myapplication.action.ADD_PHOTO";
    private static final String ACTION_REMOVE_PHOTO = "com.example.myapplication.action.REMOVE_PHOTO";

    public static final String EXTRA_PHOTOS = "com.example.myapplication.extra.PHOTOS";
    private static final String EXTRA_PHOTO = "com.example.myapplication.extra.PHOTO";

    private final MySqliteOpenHelper photosDb = new MySqliteOpenHelper(this);

    public MyIntentService() {
        super("MyIntentService");
    }

    public static void startActionLoadPhotos(Context context) {
        Intent intent = new Intent(context, MyIntentService.class);
        intent.setAction(ACTION_LOAD_PHOTOS);
        context.startService(intent);
    }

    public static void startActionAddPhoto(Context context, Photo photo) {
        Intent intent = new Intent(context, MyIntentService.class);
        intent.setAction(ACTION_ADD_PHOTO);
        intent.putExtra(EXTRA_PHOTO, photo);
        context.startService(intent);
    }


    public static void startActionDeletePhoto(Context context, Photo photo) {
        Intent intent = new Intent(context, MyIntentService.class);
        intent.setAction(ACTION_REMOVE_PHOTO);
        intent.putExtra(EXTRA_PHOTO, photo);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            Log.i(LOG_TAG, "Handling action " + action);
            if (ACTION_LOAD_PHOTOS.equals(action)) {
                handleActionLoadPhotos();
            } else if (ACTION_ADD_PHOTO.equals(action)) {
                final Photo photo = intent.getParcelableExtra(EXTRA_PHOTO);
                handleActionAddPhoto(photo);
            } else if (ACTION_REMOVE_PHOTO.equals(action)) {
                final Photo photo = intent.getParcelableExtra(EXTRA_PHOTO);
                handleActionRemovePhoto(photo);
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionLoadPhotos() {
        Retrofit jsonplaceholderRetrofit = new Retrofit.Builder()
                .baseUrl("http://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Retrofit viaJsonplaceholderRetrofit = new Retrofit.Builder()
                .baseUrl("http://via.placeholder.com/")
                .build();

        File picturesDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File albumDirectory = new File(picturesDirectory, "album");
        try {
            JsonPlaceholderApi api = jsonplaceholderRetrofit.create(JsonPlaceholderApi.class);
            JsonPlaceholderApi imageApi = viaJsonplaceholderRetrofit.create(JsonPlaceholderApi.class);
            Response<List<JsonPlaceholderPhoto>> photos = api.getPhotos().execute();
            int index = 0;

            Map<Long, Photo> idToPhoto = new HashMap<>();
            for (Photo photo : this.photosDb.getPhotos()) {
                idToPhoto.put(photo.id, photo);
            }

            for (JsonPlaceholderPhoto jphoto : photos.body()) {
                if (index == 50)
                    break;
                int lastIndexOfSlash = jphoto.url.lastIndexOf('/');
                String imageName = jphoto.url.substring(lastIndexOfSlash);
                String fileName = imageName + ".png";
                File file = new File(albumDirectory, fileName);
                if (!file.exists()) {
                    byte[] image = imageApi.getImage(imageName).execute().body().bytes();
                    try (OutputStream os = new FileOutputStream(file)) {
                        os.write(image, 0, image.length);
                    }
                }

                if (!idToPhoto.containsKey(jphoto.id)) {
                    Photo photo = new Photo(jphoto.id, jphoto.title, fileName);
                    this.photosDb.addPhoto(photo);
                }

                index += 1;
            }

            this.broadcastPhotos();
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error while fetching photos", e);
        }
    }

    private void broadcastPhotos() {
        Intent intent = new Intent(ACTION_LOAD_PHOTOS);
        intent.putParcelableArrayListExtra(EXTRA_PHOTOS, new ArrayList<Parcelable>(this.photosDb.getPhotos()));
        this.sendBroadcast(intent);
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     *
     * @param photo
     */
    private void handleActionAddPhoto(Photo photo) {
        this.photosDb.addPhoto(photo);
    }

    private void handleActionRemovePhoto(Photo photo) {
        this.photosDb.removePhoto(photo);
        this.broadcastPhotos();
    }

    @Override
    public void onCreate() {
        Log.i(LOG_TAG, "Creating service");
        super.onCreate();
    }

    @Override
    public void onStart(@Nullable Intent intent, int startId) {
        Log.i(LOG_TAG, "Starting service");
        super.onStart(intent, startId);
    }

    @Override
    public void onDestroy() {
        Log.i(LOG_TAG, "Destroying service");
        super.onDestroy();
    }
}
