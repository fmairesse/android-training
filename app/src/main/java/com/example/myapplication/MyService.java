package com.example.myapplication;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class MyService extends Service {
    private static final String LOG_TAG = "SERVICE";

    public static final int LOAD_PHOTOS_MESSAGE = 1;
    public static final int ADD_PHOTO_MESSAGE = 2;

    private final Executor executor = Executors.newSingleThreadExecutor();
    private final MySqliteOpenHelper photosDb = new MySqliteOpenHelper(this);

    public class Binder extends android.os.Binder {
        public void loadPhotos(final Handler handler) {
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    Log.i(LOG_TAG, "My thread is: " + Thread.currentThread().getName());
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    Message message = handler.obtainMessage(LOAD_PHOTOS_MESSAGE, photosDb.getPhotos());
                    handler.sendMessage(message);
                }
            });
        }

        public void addPhoto(final Photo photo, final Handler handler) {
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    photosDb.addPhoto(photo);
                    Message message = handler.obtainMessage(ADD_PHOTO_MESSAGE);
                    handler.sendMessage(message);
                }
            });
        }
    }

    public MyService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new Binder();
    }

    @Override
    public void onCreate() {
        Log.i(LOG_TAG, "Creating service");
        super.onCreate();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(LOG_TAG, "Starting service");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.i(LOG_TAG, "Destroying service");
        super.onDestroy();
    }
}
