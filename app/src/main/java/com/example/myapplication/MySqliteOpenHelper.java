package com.example.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

class MySqliteOpenHelper extends SQLiteOpenHelper {
    public static final String TABLE_PHOTO = "photo";
    public static final String COL_ID = "id";
    public static final String COL_TITLE = "title";
    public static final String COL_FILENAME = "fileName";

    private static final String DB_NAME = "photos.db";
    private static final int DB_VERSION = 1;
    private static final String LOG_TAG = "SQLITE_HELPER";
    private static final Collection<Photo> PHOTOS = Arrays.asList(
            new Photo(1, "accusamus beatae ad facilis cum similique qui sunt", "92c952.png"),
            new Photo(2, "reprehenderit est deserunt velit ipsam", "771796.png"),
            new Photo(12, "officia porro iure quia iusto qui ipsa ut modi", "24f355.png")
    );

    public MySqliteOpenHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i(LOG_TAG, "Creating DB");
        this.initTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i(LOG_TAG, "Upgrading DB");
        db.execSQL(String.format("DROP TABLE %s", TABLE_PHOTO));
        this.initTable(db);
    }

    public void addPhoto(Photo photo) {
        this.addPhoto(photo, this.getWritableDatabase());
    }

    public void removePhoto(Photo photo) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_PHOTO, COL_ID+"=?", new String[] {Long.toString(photo.id)});
    }

    private void addPhoto(Photo photo, SQLiteDatabase db) {
        ContentValues photoValues = new ContentValues();
        if (photo.id >= 0) {
            photoValues.put(COL_ID, photo.id);
        }
        photoValues.put(COL_FILENAME, photo.fileName);
        photoValues.put(COL_TITLE, photo.title);

        db.insert(TABLE_PHOTO, null, photoValues);
    }

    public Collection<Photo> getPhotos() {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] columns = {
                MySqliteOpenHelper.COL_ID,
                MySqliteOpenHelper.COL_TITLE,
                MySqliteOpenHelper.COL_FILENAME
        };
        Cursor cursor = db.query(MySqliteOpenHelper.TABLE_PHOTO, columns, null, null, null, null, MySqliteOpenHelper.COL_ID);
        Collection<Photo> photos = new ArrayList<>();
        if (cursor.moveToFirst()) {
            final int idIndex = cursor.getColumnIndex(MySqliteOpenHelper.COL_ID);
            final int idTitle = cursor.getColumnIndex(MySqliteOpenHelper.COL_TITLE);
            final int idFileName = cursor.getColumnIndex(MySqliteOpenHelper.COL_FILENAME);
            do {
                Photo photo = new Photo(
                        cursor.getInt(idIndex),
                        cursor.getString(idTitle),
                        cursor.getString(idFileName)
                );
                photos.add(photo);
            } while(cursor.moveToNext());
        }

        return photos;
    }

    private void initTable(SQLiteDatabase db) {
        db.execSQL(
                String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT, %s TEXT)",
                TABLE_PHOTO, COL_ID, COL_TITLE, COL_FILENAME));
        for (Photo photo : PHOTOS) {
            this.addPhoto(photo, db);
        }
    }
}
