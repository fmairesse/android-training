package com.example.myapplication;

import android.os.Parcel;
import android.os.Parcelable;

public class Photo implements Parcelable {
    public long id;
    public String title;
    public String fileName;

    public Photo(long id, String title, String fileName) {
        this.id = id;
        this.title = title;
        this.fileName = fileName;
    }

    protected Photo(Parcel in) {
        id = in.readLong();
        title = in.readString();
        fileName = in.readString();
    }

    public static final Creator<Photo> CREATOR = new Creator<Photo>() {
        @Override
        public Photo createFromParcel(Parcel in) {
            return new Photo(in);
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(title);
        dest.writeString(fileName);
    }
}
