package com.example.myapplication;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class PlaygroundActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String LOG_TAG = "MAIN";
    private static final int WRITE_STORAGE_REQUEST_ID = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        log("Creating activity");
        setContentView(R.layout.activity_main);
    }

    @Override
    public void onClick(View v) {
        log("Click");
        switch (v.getId()) {
            case R.id.writeButton:
                this.onWriteButtonClicked();
                break;
            case R.id.albumButton:
                this.onAlbumButtonClicked();
                break;
            case R.id.startServiceButton:
                MyIntentService.startActionLoadPhotos(this);
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case WRITE_STORAGE_REQUEST_ID: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    writeToStorage();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    Log.w(LOG_TAG, "Permission to write to storage has been refused");
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    private void onWriteButtonClicked() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Please allow me to write the external storage")
                        .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(PlaygroundActivity.this,
                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        WRITE_STORAGE_REQUEST_ID);
                            }
                        });
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        WRITE_STORAGE_REQUEST_ID);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            writeToStorage();
        }
    }

    private void onAlbumButtonClicked() {
        Intent intent = new Intent(this, AlbumActivity.class);
        File picturesDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File albumDirectory = new File(picturesDirectory, "album");
        intent.putExtra(Intents.EXTRA_ALBUM_DIRECTORY, albumDirectory.getAbsolutePath());
        this.startActivity(intent);
    }

    private static void writeToStorage() {
        File f = new File(Environment.getExternalStorageDirectory(), "toto.txt");
        log("Writing file: " + f.getAbsolutePath());
        try (FileWriter writer = new FileWriter(f, true)){
            writer.write("toto fait du vélo\n");
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error while writing file", e);
        }
    }

    private static void log(String message) {
        Log.i(LOG_TAG, message);
    }
}
